# Teste Gympass

Esse repostório contém a aplicação e (praticamente) todas as suas dependêndencias.

Porém, ainda é necessário instalar o [Docker](https://www.docker.com/community-edition) e o [docker-compose](https://docs.docker.com/compose/).

Uma vez que ambos estejam instalados podemos continuar com a instalação.

Em primeiro lugar, clone este repositório e acesse seu diretório:
```sh
$ cd grandprix
```

Agora é necessário criar os conteineres. Apenas rode o comando abaixo e deixe o resto para o Docker:
```sh
$ docker-compose build
```

Esse comando pode levar um tempo para finalizar.

Após finalizar, basta rodar a aplicação

```sh
$ docker-compose run grandprix mix run
```
