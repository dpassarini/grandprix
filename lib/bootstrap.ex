defmodule Grandprix.Bootstrap do
  def inicio() do
    resultado_pid = GenServer.whereis(:resultado_corrida)
    dominio_pilotos_pid = GenServer.whereis(:dominio_pilotos)
    arq = File.stream!("corrida.log")

    arq
    |> Stream.drop(1)
    |> Enum.map(fn linha ->
      ln = String.split(String.replace(String.trim(linha), "\t", " "), " ", trim: true)
      piloto = linha_para_piloto(ln)
      GenServer.cast(dominio_pilotos_pid, {:adicionar_piloto, piloto})
      GenServer.cast(resultado_pid, {:registrar_piloto, piloto})
    end)

    GenServer.cast(resultado_pid, :registrar_duracao_prova)
    GenServer.cast(resultado_pid, :registrar_media_por_piloto)

    resultado = GenServer.call(resultado_pid, :resultado)
    tabela_pilotos = GenServer.call(dominio_pilotos_pid, :pilotos)
    Grandprix.ExibirResultado.saida(resultado, tabela_pilotos)
  end

  defp linha_para_piloto(linha) do
    %Grandprix.Piloto{
      nome: Enum.at(linha, 3),
      numero: String.to_integer(Enum.at(linha, 1)),
      volta_atual: String.to_integer(Enum.at(linha, 4)),
      hora_volta: Time.from_iso8601!(Enum.at(linha, 0)),
      tempo_volta: converter_tempo_volta(Enum.at(linha, 5)),
      melhor_volta: String.to_integer(Enum.at(linha, 4)),
      velocidade_media_total: String.to_float(String.replace(Enum.at(linha, 6), ",", "."))
    }
  end

  defp converter_tempo_volta(tempo_em_minutos) do
    [minutos, segundos] = String.split(tempo_em_minutos, ":")
    String.to_integer(minutos) * 60 + String.to_float(segundos)
  end
end