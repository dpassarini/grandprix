defmodule Grandprix.Resultado do
  defstruct melhor_volta: nil,
            posicao_chegada: [],
            prova_finalizada: false,
            voltas_completas: %{},
            tempo_total: 0.0,
            primeira_volta: nil,
            ultima_volta: nil,
            tempo_total: nil,
            melhor_volta_por_piloto: %{},
            velocidade_media_por_piloto: %{},
            velocidade_media_acumulada_por_piloto: %{},
            hora_volta_vencedor: nil

  def calcular_tempo_de_prova(resultado) do
    primeira_volta_hora = resultado.primeira_volta.hora_volta
    tempo_primeira_volta = resultado.primeira_volta.tempo_volta
    fim = resultado.ultima_volta.hora_volta

    inicio =
      Time.add(primeira_volta_hora, Kernel.trunc(tempo_primeira_volta * 1000), :millisecond)

    {:ok, duracao_prova} =
      Timex.Duration.to_time(
        Timex.Duration.from_milliseconds(Timex.diff(fim, inicio, :milliseconds))
      )

    duracao_prova
  end

  def calcular_media_por_piloto(resultado) do
    media_atualizada =
      for {_piloto, media} <- resultado.velocidade_media_acumulada_por_piloto do
        {piloto, media_acumulado, voltas} = media
        {piloto, media_acumulado / voltas}
      end

    media_atualizada
  end
end
