defmodule Grandprix.Application do
  use Application

  def start(_type, _args) do
    children = [
      {Grandprix.RegistrarResultado, []},
      {Grandprix.Dominio.Pilotos, []}
    ]

    opts = [strategy: :one_for_one, name: Grandprix.Supervisor]
    Supervisor.start_link(children, opts)

    Grandprix.Bootstrap.inicio()
    {:ok, self()}
  end
end
