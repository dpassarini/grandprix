defmodule Grandprix.Piloto do
  defstruct numero: nil,
            nome: nil,
            melhor_volta: nil,
            tempo_volta: nil,
            volta_atual: nil,
            velocidade_media_total: nil,
            hora_volta: nil
end
