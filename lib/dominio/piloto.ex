defmodule Grandprix.Dominio.Pilotos do
  use GenServer

  def init(init_arg) do
    {:ok, init_arg}
  end

  def start_link(_init_arg) do
    GenServer.start_link(__MODULE__, %{}, name: :dominio_pilotos)
  end

  def handle_cast({:adicionar_piloto, piloto}, tabela_pilotos) do
    pilotos = Map.put(tabela_pilotos, piloto.numero, piloto.nome)
    {:noreply, pilotos}
  end

  def handle_call(:pilotos, _from, tabela_pilotos) do
    {:reply, tabela_pilotos, tabela_pilotos}
  end
end
