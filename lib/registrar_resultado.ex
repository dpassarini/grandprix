defmodule Grandprix.RegistrarResultado do
  alias Grandprix.Resultado
  use GenServer

  @total_voltas 4

  # res = %Grandprix.Resultado{}
  # res1 = %Grandprix.Resultado{res | ["teste" | res.posicao_chegada]}

  def init(init_arg) do
    {:ok, init_arg}
  end

  def start_link(_init_arg) do
    GenServer.start_link(__MODULE__, %Resultado{}, name: :resultado_corrida)
  end

  def handle_cast({:registrar_piloto, piloto}, resultado_prova) do
    prova =
      resultado_prova
      |> registrar_posicao(piloto)
      |> registrar_melhor_volta(piloto)
      |> registrar_melhor_volta_por_piloto(piloto)
      |> registrar_final_da_prova(piloto)
      |> registrar_voltas_limites(piloto)
      |> registrar_medias_acumuladas(piloto)

    {:noreply, prova}
  end

  def handle_cast(:registrar_duracao_prova, estado_da_prova) do
    {:noreply,
     %Resultado{
       estado_da_prova
       | tempo_total: Grandprix.Resultado.calcular_tempo_de_prova(estado_da_prova)
     }}
  end

  def handle_cast(:registrar_media_por_piloto, estado_da_prova) do
    {:noreply,
     %Resultado{
       estado_da_prova
       | velocidade_media_por_piloto:
           Grandprix.Resultado.calcular_media_por_piloto(estado_da_prova)
     }}
  end

  def handle_call(:resultado, _from, estado) do
    {:reply, estado, estado}
  end

  defp prova_terminada?(piloto) do
    piloto.volta_atual == @total_voltas
  end

  defp registrar_voltas_limites(estado_da_prova, piloto) do
    resultado_prova =
      if estado_da_prova.primeira_volta == nil do
        %Resultado{estado_da_prova | primeira_volta: piloto}
      else
        estado_da_prova
      end

    %Resultado{resultado_prova | ultima_volta: piloto}
  end

  defp registrar_posicao(estado_da_prova, piloto) do
    if prova_terminada?(piloto) do
      %Resultado{estado_da_prova | posicao_chegada: [piloto | estado_da_prova.posicao_chegada]}
    else
      estado_da_prova
    end
  end

  defp registrar_melhor_volta(estado_da_prova, piloto) do
    case estado_da_prova.melhor_volta do
      nil ->
        %Resultado{
          estado_da_prova
          | melhor_volta: {piloto.numero, piloto.tempo_volta, piloto.volta_atual}
        }

      {_piloto_mais_rapido, volta_mais_rapida_atual, _volta_mais_rapida} ->
        if piloto.tempo_volta < volta_mais_rapida_atual do
          %Resultado{
            estado_da_prova
            | melhor_volta: {piloto.numero, piloto.tempo_volta, piloto.volta_atual}
          }
        else
          estado_da_prova
        end
    end
  end

  defp registrar_melhor_volta_por_piloto(estado_da_prova, piloto) do
    if estado_da_prova.melhor_volta_por_piloto[piloto.numero] == nil do
      %Resultado{
        estado_da_prova
        | melhor_volta_por_piloto:
            Map.put(
              estado_da_prova.melhor_volta_por_piloto,
              piloto.numero,
              {piloto.tempo_volta, piloto.volta_atual}
            )
      }
    else
      {melhor_volta_atual, _numero_piloto} =
        estado_da_prova.melhor_volta_por_piloto[piloto.numero]

      if piloto.tempo_volta < melhor_volta_atual do
        %Resultado{
          estado_da_prova
          | melhor_volta_por_piloto:
              Map.put(
                estado_da_prova.melhor_volta_por_piloto,
                piloto.numero,
                {piloto.tempo_volta, piloto.volta_atual}
              )
        }
      else
        estado_da_prova
      end
    end
  end

  defp registrar_final_da_prova(estado_da_prova, piloto) do
    if estado_da_prova.prova_finalizada do
      estado_da_prova
    else
      resultado = %Resultado{
        estado_da_prova
        | voltas_completas:
            Map.put(
              estado_da_prova.voltas_completas,
              piloto.numero,
              {piloto.volta_atual, piloto.hora_volta}
            )
      }

      %Resultado{
        resultado
        | prova_finalizada: piloto.volta_atual == @total_voltas,
          hora_volta_vencedor: piloto.hora_volta
      }
    end
  end

  defp registrar_medias_acumuladas(estado_da_prova, piloto) do
    media_atualizada =
      if estado_da_prova.velocidade_media_acumulada_por_piloto[piloto.numero] == nil do
        {piloto.numero, piloto.velocidade_media_total, piloto.volta_atual}
      else
        {_piloto, media_atual, _volta} =
          estado_da_prova.velocidade_media_acumulada_por_piloto[piloto.numero]

        {piloto.numero, piloto.velocidade_media_total + media_atual, piloto.volta_atual}
      end

    %Resultado{
      estado_da_prova
      | velocidade_media_acumulada_por_piloto:
          Map.put(
            estado_da_prova.velocidade_media_acumulada_por_piloto,
            piloto.numero,
            media_atualizada
          )
    }
  end
end
