FROM elixir:1.9.2-slim

RUN apt-get update && apt-get install -qq -y nodejs inotify-tools

ENV INSTALL_PATH /app

RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH

COPY . .

RUN mix local.hex --force

RUN mix local.rebar --force

RUN mix deps.get

RUN mix compile
