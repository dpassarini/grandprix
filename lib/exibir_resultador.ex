defmodule Grandprix.ExibirResultado do
  def saida(resultado, pilotos_dominio) do
    IO.puts("=====================================================================")
    IO.puts(" POSIÇÃO DE CADA PILOTO ")
    IO.puts("=====================================================================")

    vencedores(Enum.reverse(resultado.posicao_chegada), pilotos_dominio)

    IO.puts(" ")
    IO.puts("=====================================================================")
    IO.puts(" RELAÇÃO DE PILOTOS (numero - nome do piloto) ")
    IO.puts("=====================================================================")

    pilotos(pilotos_dominio)

    IO.puts(" ")
    IO.puts("=====================================================================")
    IO.puts(" QUANTIDADE DE VOLTAS COMPLETAS ATÉ A PROVA TERMINAR ")
    IO.puts("=====================================================================")
    quantidade_de_voltas_completadas(resultado.voltas_completas, pilotos_dominio)

    IO.puts(" ")
    IO.puts("=====================================================================")
    IO.puts(" TEMPO TOTAL DE PROVA ")
    IO.puts("=====================================================================")
    IO.puts(resultado.tempo_total)

    IO.puts(" ")
    IO.puts("=====================================================================")
    IO.puts(" MELHOR VOLTA DE CADA PILOTO ")
    IO.puts("=====================================================================")
    melhor_volta_por_piloto(resultado.melhor_volta_por_piloto, pilotos_dominio)

    IO.puts(" ")
    IO.puts("=====================================================================")
    IO.puts(" MELHOR VOLTA DA PROVA ")
    IO.puts("=====================================================================")
    melhor_volta_da_prova(resultado.melhor_volta, pilotos_dominio)

    IO.puts(" ")
    IO.puts("=====================================================================")
    IO.puts(" VELOCIDADE MEDIA POR PILOTO ")
    IO.puts("=====================================================================")
    velocidade_media_por_piloto(resultado.velocidade_media_por_piloto, pilotos_dominio)

    IO.puts(" ")
    IO.puts("=====================================================================")
    IO.puts(" CHEGADA DE CADA PILOTO ")
    IO.puts("=====================================================================")
    chegada_de_cada_piloto(resultado, pilotos_dominio)
  end

  defp vencedores(lista_de_pilotos, pilotos_dominio, colocacao \\ 1) do
    case lista_de_pilotos do
      [] ->
        :ok

      [piloto | pilotos_restantes] ->
        IO.puts("#{colocacao} - #{pilotos_dominio[piloto.numero]}")
        vencedores(pilotos_restantes, pilotos_dominio, colocacao + 1)
    end
  end

  defp pilotos(pilotos_dominio) do
    for {numero, nome} <- pilotos_dominio do
      IO.puts("#{numero} - #{nome}")
    end
  end

  defp quantidade_de_voltas_completadas(voltas_completas, pilotos_dominio) do
    for {numero, {qtd_voltas, _tempo}} <- voltas_completas do
      IO.puts("#{pilotos_dominio[numero]} completou #{qtd_voltas} voltas")
    end
  end

  defp melhor_volta_por_piloto(melhores_voltas, pilotos_dominio) do
    for {numero, {tempo_volta, volta}} <- melhores_voltas do
      {:ok, tempo_volta_em_minuto} =
        Timex.Duration.to_time(Timex.Duration.from_seconds(tempo_volta))

      IO.puts(
        "#{pilotos_dominio[numero]} - volta número #{volta} - tempo de #{tempo_volta_em_minuto}"
      )
    end
  end

  defp melhor_volta_da_prova(melhor_volta, pilotos_dominio) do
    {numero, tempo_volta, volta} = melhor_volta

    {:ok, melhor_volta_em_minuto} =
      Timex.Duration.to_time(Timex.Duration.from_seconds(tempo_volta))

    IO.puts(
      "#{pilotos_dominio[numero]} - volta número #{volta} - tempo de #{melhor_volta_em_minuto}"
    )
  end

  defp velocidade_media_por_piloto(velocidades_medias, pilotos_dominio) do
    case velocidades_medias do
      [] ->
        :ok

      [{numero, tempo_medio} | pilotos_restantes] ->
        {:ok, tempo_medio_em_minuto} =
          Timex.Duration.to_time(Timex.Duration.from_seconds(tempo_medio))

        IO.puts("#{pilotos_dominio[numero]} - tempo de #{tempo_medio_em_minuto}")
        velocidade_media_por_piloto(pilotos_restantes, pilotos_dominio)
    end
  end

  defp chegada_de_cada_piloto(resultado, pilotos_dominio) do
    hora_volta_vencedor = resultado.hora_volta_vencedor

    for piloto <- resultado.posicao_chegada do
      diferenca = Timex.diff(piloto.hora_volta, hora_volta_vencedor, :milliseconds)

      if diferenca > 0.0 do
        {:ok, diferenca_em_minuto} =
          Timex.Duration.to_time(Timex.Duration.from_milliseconds(diferenca))

        IO.puts(
          "#{pilotos_dominio[piloto.numero]} chegou #{diferenca_em_minuto} depois do vencedor"
        )
      end
    end
  end
end
